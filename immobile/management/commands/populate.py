import random
import string
from datetime import date, datetime, timedelta
from django.core.management.base import BaseCommand
from immobile.models import Immobile, Advertisement, Reservation


def random_code(size=6, chars=string.ascii_uppercase + string.digits):
    return "".join(random.choice(chars) for _ in range(size))


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        immobiles_code = ["CAS", " APA", "FLA", "SOB", "KIT", "LOF"]
        
        start_date = date(2020, 1, 1)
        

        for im in range(0, 5):
            random_days = random.randint(0, random.randint(1, 30))
            random_date = start_date + timedelta(days=random_days)

            Immobile.objects.create(
                code=f'{random.choice(immobiles_code)}{im}',
                guest_limit=random.randint(1, 10),
                bathroom_count=random.randint(1, 4),
                is_pet=bool(random.getrandbits(1)),
                cleaning_fee=random.uniform(100, 250),
                activation_date=random_date,
            )


        self.stdout.write(self.style.SUCCESS("Imóveis criados"))

        plataform_names = [
            "Airbnb",
            "TripAdvisor",
            "Homestay",
            "AlugueTemporada",
            "Couchsurfing",
        ]
         
        immobiles_list_ids = Immobile.objects.values_list('id', flat=True)
        for ad in range(0, 6):

            Advertisement.objects.create(
                immobile_id=random.choice(immobiles_list_ids),
                platform_name=random.choice(plataform_names),
                platform_fee=random.uniform(10, 40)
            )
        
        self.stdout.write(self.style.SUCCESS("Anúncios criados"))

        ads_list_ids = Advertisement.objects.values_list('id', flat=True)
        
        date_start = datetime.now()

        for re in range(0, 8):
            date_end = date_start + timedelta(days=re + 10)
            Reservation.objects.create(
                code=random_code(),
                advertisement_id=random.choice(ads_list_ids),
                num_guests=random.randint(1, 10),
                total_price=random.uniform(400, 1000),
                check_in=date_start,
                check_out=date_end,
            )
        
        self.stdout.write(self.style.SUCCESS("Reservas criados"))
