from rest_framework import generics
from .models import Immobile, Advertisement, Reservation
from .serializers import ImmobileSerializer, AdvertismentSerializer, ReservationSerializer


class ImmobileList(generics.ListCreateAPIView):
    queryset = Immobile.objects.all()
    serializer_class = ImmobileSerializer


class ImmobileDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Immobile.objects.all()
    serializer_class = ImmobileSerializer


class AdvertismentList(generics.ListCreateAPIView):
    queryset = Advertisement.objects.all()
    serializer_class = AdvertismentSerializer


class AdvertismentDetail(generics.RetrieveUpdateAPIView):
    queryset = Advertisement.objects.all()
    serializer_class = AdvertismentSerializer


class ReservationtList(generics.ListCreateAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer


class ReservationDetail(generics.RetrieveDestroyAPIView):
    queryset = Reservation.objects.all()
    serializer_class = ReservationSerializer

