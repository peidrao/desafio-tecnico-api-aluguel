from django.urls import path
from . import views

urlpatterns = [
    path('immobiles/', views.ImmobileList.as_view(), name='immobile-list'),
    path('immobiles/<int:pk>/', views.ImmobileDetail.as_view(), name='immobile-detail'),

    path('ads/', views.AdvertismentList.as_view(), name='ad-list'),
    path('ads/<int:pk>/', views.AdvertismentDetail.as_view(), name='ad-detail'),

    path('reservations/', views.ReservationtList.as_view(), name='ad-list'),
    path('reservations/<int:pk>/', views.ReservationDetail.as_view(), name='ad-detail'),


]

